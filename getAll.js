"use strict";

const puppeteer = require('puppeteer');
const axios = require('axios');

let didLogin = false;
let sessionId = undefined;
let count = 0;
let browser = undefined;
let page = undefined;
let PAGE_WAIT_DURATION = 5000;

const authUrl = "https://gordiontrio.likom.com.tr/APIService/auth/";
// const authUrl = "http://localhost:2220/auth/";
const serviceUrl = "https://gordiontrio.likom.com.tr/APIService/json/reply/Request_Search_Model?SearchString=&ModelType=UI_Page&Skip=0&Take=3000";
// const serviceUrl = "http://localhost:2220/json/reply/Request_Search_Model?SearchString=&ModelType=UI_Page&Skip=0&Take=3000";

const performAuthentication = function () {
  return new Promise(function(resolve, reject) {
    console.log("Performing api authentication.");
    axios.get(authUrl, {
      params : {
        "UserName":"admin",
        "Password":"gtAdmingt321*",
        "RememberMe":"true"
      }
    })
    .then((resp) => {
      resolve(resp.data.SessionId);
    });
  });
}

const getPageIds = function () {
  return new Promise(function(resolve, reject) {
  axios.get(serviceUrl, {
        headers: {
          "X-ss-opt" : "perm",
          "X-ss-pid" : sessionId,
          "Content-Type" : "application/json; charset=utf-8"
        }})
    .then(function (response) {
      const Ids = response.data.Items.map(item => item.Id);
      resolve(Ids);
    })
    .catch(function (error) {
      // handle error
      console.log(error);
      reject(error);
    });
  });
}

const beklet = (ms, callback) => {
  return new Promise(function(resolve, reject) {
    setTimeout(() => {
      if(callback){
        callback();
      }
      resolve();
    }, ms);
  });

}

const writePassword = function () {
  console.log("Simulating login...");
  return new Promise(async function(resolve, reject) {

    let passwordInput = undefined;
    console.log("Fetching passwordInput...");
    do { passwordInput = await page.$("input[type=password]"); } while (!passwordInput);
    console.log("passwordInput fetched.")

    await passwordInput.type("gtAdmingt321*");

    let signInButton = undefined;
    console.log("Fetching signInButton...");
    do { signInButton = await page.$("button"); } while (!signInButton);
    console.log("signInButton fetched.")

    await signInButton.click();
    await beklet(5000, resolve);
  });
}

const openSelect = function () {
  console.log("Opening company list...");
  return new Promise(async function(resolve, reject) {

    let selectDiv=undefined;
    console.log("Fetching selectDiv...");
    do { selectDiv = await page.$("div[tabindex='0']"); } while (!selectDiv);
    console.log("selectDiv fetched.")

    // const selectDiv = await page.$("div[tabindex='0']");
    await selectDiv.click();
    await beklet(2000, resolve);
  });
}

const chooseFirstCompany = function () {
  console.log("Choosing the first company on the list...");
  return new Promise(async function(resolve, reject) {
    // const optionSpan = await page.$('span[tabindex="1"]');
    let optionSpan = undefined;
    console.log("Fetching optionSpan...");
    do { optionSpan = await page.$('span[tabindex="1"]'); } while (!optionSpan);
    console.log("optionSpan fetched.")

    await optionSpan.click();

    // const loadCompanyButton = await page.$("button");
    let loadCompanyButton = undefined;
    console.log("Fetching loadCompanyButton...");
    do { loadCompanyButton = await page.$("button"); } while (!loadCompanyButton);
    console.log("loadCompanyButton fetched.")

    await loadCompanyButton.click();
    await beklet(2000, resolve);
  });
}

const simulateLogin = function () {
  return new Promise(async function(resolve, reject) {
    await writePassword(page);
    await openSelect(page);
    await chooseFirstCompany(page);
    didLogin = true;
    await beklet(5000, resolve);
  });
}

const getThumbNail = async function(idArray) {

  const pageId = idArray.shift();

  if (pageId) {

    const url = "https://gordiontrio.likom.com.tr/trio/#/Page/"+pageId+"/0";
    // const url = "http://localhost:81/#/Page/"+pageId+"/0";
    console.log(`Browsing ${url} ...`);

    await page.goto(url, {timeout : 0});
    await beklet(PAGE_WAIT_DURATION);

    if (!didLogin){
      await simulateLogin(page);
    }

    const fileName = pageId +"_0"+".jpg";
    console.log("Saving file : " + fileName);

    await page.screenshot({
      path : "./thumbnails_all/" + fileName,
      fullPage: true
    });

    count++;

    if (count==50){
      console.log("Closing browser...");
      await browser.close();
      page = undefined;
      browser = undefined;
      didLogin = false;
      count = 0;
      await beklet(1000);
      await resetBrowserAndPage();
    }

    getThumbNail(idArray);

  } else {
    console.log("Closing browser...");
    browser.close();
    return;
  }

}

const resetBrowserAndPage = function () {

  return new Promise(async function(resolve, reject) {

    console.log("Starting browser...");
    // const browser = await puppeteer.launch({timeout : 0, headless: false});
    browser = await puppeteer.launch({timeout : 0}); // timeoutu devreden çıkart.
    page = await browser.newPage();
    // page.setViewport({width : 1920, height: 1080});

    await beklet(1000, resolve);

  });

}

const initBrowser = async function () {

  console.log("Sayfa bekleme süresi : " + PAGE_WAIT_DURATION);

  sessionId = await performAuthentication();

  // Görüntülerken hata veren sayfalar.
  // const idArray = [
  //   "2fc67811-208b-486d-9f39-64de7a41d185",
  //   "4bedbd2d-c0e1-4266-a519-43b9d81b3021",
  //   "0a575790-eceb-4514-989a-12899036edc3",
  //   "0fa1754e-eac9-4215-a02f-fc27f9db1be2",
  //   "0fac8e36-628d-4032-9b31-c51fdf693a55",
  //   "1c112c02-57ca-49f0-bd1d-eba0dc6effcc",
  //   "1c735736-f712-45ee-811f-3f94c95d7517"
  // ]

  const idArray = await getPageIds();
  // idArray.shift();

  // const url = "https://gordiontrio.likom.com.tr/trio/#/Page/"+PAGE_ID+"/"+VIEW_ID;
  //"http://localhost:82/#/Page/"+PAGE_ID+"/"+VIEW_ID;

  await resetBrowserAndPage();

  getThumbNail(idArray);
}

module.exports = initBrowser();
