"use strict";

const puppeteer = require('puppeteer');
const fs = require('fs');

let didLogin = false;
let fileNo = 0;

const urlArray = [
                  "http://localhost:81/#/Page/7c31eea2-b372-45b3-ad43-5aab7a302b1f/0/",
                  "http://localhost:81/#/Page/f5531e7c-1625-4eb1-b727-7375304f003d/0/",
                  "http://localhost:81/#/Page/4ba9a69f-93d4-4c58-8234-66bc02226286/0",
                  "http://localhost:81/#/Page/fe3dc924-16fa-444d-9fe8-5ce726d71254/0",
                  "https://www.yahoo.com",
                  "https://www.google.com",
                  "http://www.hurriyet.com.tr",
                  "https://www.sozcu.com.tr",
                  "https://www.sahibinden.com",
                  "https://www.hepsiburada.com"
                 ];

const beklet = (ms, callback) => {
  setTimeout(() => {
    if(callback){
      callback();
    }
  }, ms);
}

const writePassword = function (page) {
  console.log("Simulating login...");
  return new Promise(async function(resolve, reject) {
    const passwordInput = await page.$("input[type=password]");
    await passwordInput.type("gtAdmingt321*");
    const signInButton = await page.$("button");
    await signInButton.click();
    beklet(3000, resolve);
  });
}

const openSelect = function (page) {
  console.log("Opening company list...");
  return new Promise(async function(resolve, reject) {
    const selectDiv = await page.$("div[tabindex='0']");
    await selectDiv.click();
    beklet(1000, resolve);
  });
}

const chooseFirstCompany = function (page) {
  console.log("Choosing the first company on the list...");
  return new Promise(async function(resolve, reject) {
    const optionSpan = await page.$('span[tabindex="1"]');
    await optionSpan.click();
    const loadCompanyButton = await page.$("button");
    await loadCompanyButton.click();
    beklet(1000, resolve);
  });
}

const simulateLogin = function (page) {
  return new Promise(async function(resolve, reject) {
    await writePassword(page);
    await openSelect(page);
    await chooseFirstCompany(page);
    didLogin = true;
    beklet(1000, resolve);
  });
}

const getThumbNails = async function(page, arr, browser) {
  const url = arr.shift();
  if (!url) {
    await browser.close();
    return;
  }

  console.log(`Browsing ${url} ...`);

  await page.goto(url);
  if (!didLogin){
    await simulateLogin(page);
  }

  const fileName = "file_"+(++fileNo)+".png";

  await page.screenshot({
    path : "./thumbnails/" + fileName,
    fullPage: true
  });

  setTimeout(() => {
    getThumbNails(page, arr, browser);
  }, 1000);

}

const initBrowser = async function () {
  // const browser = await puppeteer.launch({headless: false});
  const browser = await puppeteer.launch();
  const page = await browser.newPage();
  page.setViewport({width : 1024, height: 768});
  getThumbNails(page, urlArray, browser);
}

initBrowser();
