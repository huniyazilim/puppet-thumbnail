"use strict";

const puppeteer = require('puppeteer');
const fs = require('fs');

let didLogin = false;
let fileNo = 0;

const beklet = (ms, callback) => {
  return new Promise(function(resolve, reject) {
    setTimeout(() => {
      if(callback){
        callback();
      }
      resolve();
    }, ms);
  });

}

const writePassword = function (page) {
  console.log("Simulating login...");
  return new Promise(async function(resolve, reject) {

    let passwordInput = undefined;
    console.log("Fetching passwordInput...");
    do { passwordInput = await page.$("input[type=password]"); } while (!passwordInput);
    console.log("passwordInput fetched.")

    await passwordInput.type("gtAdmingt321*");

    let signInButton = undefined;
    console.log("Fetching signInButton...");
    do { signInButton = await page.$("button"); } while (!signInButton);
    console.log("signInButton fetched.")

    await signInButton.click();
    await beklet(1000, resolve);
  });
}

const openSelect = function (page) {
  console.log("Opening company list...");
  return new Promise(async function(resolve, reject) {

    let selectDiv=undefined;
    console.log("Fetching selectDiv...");
    do { selectDiv = await page.$("div[tabindex='0']"); } while (!selectDiv);
    console.log("selectDiv fetched.")

    // const selectDiv = await page.$("div[tabindex='0']");
    await selectDiv.click();
    await beklet(1000, resolve);
  });
}

const chooseFirstCompany = function (page) {
  console.log("Choosing the first company on the list...");
  return new Promise(async function(resolve, reject) {
    // const optionSpan = await page.$('span[tabindex="1"]');
    let optionSpan = undefined;
    console.log("Fetching optionSpan...");
    do { optionSpan = await page.$('span[tabindex="1"]'); } while (!optionSpan);
    console.log("optionSpan fetched.")

    await optionSpan.click();

    // const loadCompanyButton = await page.$("button");
    let loadCompanyButton = undefined;
    console.log("Fetching loadCompanyButton...");
    do { loadCompanyButton = await page.$("button"); } while (!loadCompanyButton);
    console.log("loadCompanyButton fetched.")

    await loadCompanyButton.click();
    await beklet(1000, resolve);
  });
}

const simulateLogin = function (page) {
  return new Promise(async function(resolve, reject) {
    await writePassword(page);
    await openSelect(page);
    await chooseFirstCompany(page);
    didLogin = true;
    await beklet(1000, resolve);
  });
}

const getThumbNail = async function(page, url, pageId, viewId, browser) {
  console.log(`Browsing ${url} ...`);

  await page.goto(url, {timeout : 0});
  await beklet(1000);

  if (!didLogin){
    await simulateLogin(page);
  }

  const fileName = pageId +"_"+viewId+".jpg";
  console.log("Saving file : " + fileName);

  await page.screenshot({
    path : "./thumbnails/" + fileName,
    fullPage: true
  });

  console.log("Closing browser...");
  browser.close();
  return;

}

const initBrowser = async function () {
  const PAGE_ID = process.argv[2];
  const VIEW_ID = process.argv[3];

  if (!PAGE_ID || !VIEW_ID){
    console.log("Too few parameters");
    return;
  }

  const url = "https://gordiontrio.likom.com.tr/trio/#/Page/"+PAGE_ID+"/"+VIEW_ID;
  //"http://localhost:82/#/Page/"+PAGE_ID+"/"+VIEW_ID;

  const browser = await puppeteer.launch({timeout : 0, headless: false});
  // const browser = await puppeteer.launch({timeout : 0}); // timeoutu devreden çıkart.
  const page = await browser.newPage();
  // page.setViewport({width : 1920, height: 1080});
  getThumbNail(page, url, PAGE_ID, VIEW_ID, browser);
}

module.exports = initBrowser();
